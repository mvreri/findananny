<div class="row">
		
		
		
		
			<!-- Raw Links -->
			<div class="col-md-12 col-sm-12 clearfix hidden-xs">
		
				<ul class="list-inline links-list pull-right">	
					<li>
						<a href="../login.php?view=login">
							Log Out <i class="entypo-logout right"></i>
						</a>
					</li>
				</ul>
		
			</div>
		
		</div>
		
		<hr />
		
					<ol class="breadcrumb bc-3" >
								<li>
						<a href="index.html"><i class="fa-home"></i>Home</a>
					</li>
							<li>
		
									<a href="tables-main.html"><?php echo $breadcrumb1; ?></a>
							</li>
						<li class="active">
		
									<strong><?php echo $breadcrumb2; ?></strong>
							</li>
							</ol>
					
		<h2>Nannies & Housekeepers</h2>
		

				
		<h3>Available </h3>
		<br />
		
		<table class="table table-bordered datatable" id="table-nannies">
			<thead>
				<tr>
					<th>Name</th>
					<th>Experience</th>
					<th>Age</th>
					<th>CV</th>
					<th>Photo</th>
				</tr>
			</thead>
			<tbody>
			<?php
			$res=dbQuery("SELECT n.name name, n.experience exp, n.cv resumee, n.photo picha, s.minsalary minsal, s.maxsalary maxsal, sp.name spec, YEAR(CURDATE())-n.yob age
							FROM tbl_nanny n
							INNER JOIN tbl_salary s
							ON s.id=n.salaryid
							INNER JOIN tbl_specialization sp
							ON sp.id = n.specializationid
							WHERE n.availability=1
							ORDER BY n.id desc;");
							while ($row=dbFetchAssoc($res)){
								extract($row);
											
                                                ?>
				<tr class="odd gradeX">
					<td><?php echo $name; ?></td>
					<td><?php echo $exp; ?></td>
					<td><?php echo $age; ?></td>					
					<td><a href="../decrypteduploadscv/<?php echo $resumee; ?>"><?php echo $resumee; ?></a></td>
					<td><img src="../decrypteduploads/<?php echo $picha; ?>" width="175" height="200" /></td>
				</tr>
				 <?php
					}
				?>
			</tbody>
			<tfoot>
				<tr>
					<th>Name</th>
					<th>Experience</th>
					<th>Age</th>
					<th>CV</th>
					<th>Photo</th>
				</tr>
			</tfoot>
		</table>
		<script type="text/javascript">
			jQuery(document).ready(function($)
			{
				var table = $("#table-nannies").dataTable({
					"sPaginationType": "bootstrap",
					"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
					"oTableTools": {
					},
					
				});
			});
				
		</script>