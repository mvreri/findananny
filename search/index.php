<?php
require_once '../library/config.php';

$errorMessage = (isset($_GET['error']) && $_GET['error'] != '') ? $_GET['error'] : '&nbsp;';

$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

switch ($view) {
	case 'search' :
		$heading = 'Bureau | Start';
		$content = 'search.php';
		$pageTitle = 'Find-A-Nanny | Search For A Nanny In Kenya';
		break;
		
	case 'results' :
		$heading = 'Bureau | Start';
		$content = 'results.php';
		$pageTitle = 'Find-A-Nanny | Search For A Nanny In Kenya';
		break;
		
	
	default :
		$heading = 'Bureau | Start';
		$breadcrumb1 = 'Personnel';
		$breadcrumb2 = 'List All';
		$content = 'search.php';
		$pageTitle = 'Find-A-Nanny | Search For A Nanny In Kenya';
}
	

require_once 'template.php';
?>