<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />

	<title><?php echo $pageTitle; ?></title>

	<link rel="stylesheet" href="../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="../assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="../assets/css/bootstrap.css">
	<link rel="stylesheet" href="../assets/css/neon-core.css">
	<link rel="stylesheet" href="../assets/css/neon-theme.css">
	<link rel="stylesheet" href="../assets/css/neon-forms.css">
	<link rel="stylesheet" href="../assets/css/custom.css">
	<link rel="stylesheet" href="../assets/css/skins/white.css">

	<script src="../assets/js/jquery-1.11.0.min.js"></script>
	<script>$.noConflict();</script>

	<!--[if lt IE 9]><script src="../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->


</head>
<body class="page-body" data-url="http://black.co.ke/nanny">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<div class="sidebar-menu">

		<div class="sidebar-menu-inner">
			
			<header class="logo-env">

				<!-- logo -->
				<div class="logo">
					<a href="../">
						<img src="../assets/images/logo@2x.png" width="120" alt="" />
					</a>
				</div>

				<!-- logo collapse icon -->
				<div class="sidebar-collapse">
					<a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
						<i class="entypo-menu"></i>
					</a>
				</div>

								
				<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
				<div class="sidebar-mobile-menu visible-xs">
					<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
						<i class="entypo-menu"></i>
					</a>
				</div>

			</header>
			
									
			<ul id="main-menu" class="main-menu">
				<!-- add class "multiple-expanded" to allow multiple submenus to open -->
				<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
				<li>
					<a href="../" >
						<i class="entypo-home"></i>
						<span class="title">Home</span>
					</a>
				</li>
				<li>
					<a href="?view=search">
						<i class="entypo-find"></i>
						<span class="title">Search</span>
					</a>
				</li>
				
				
				
			</ul>
			
		</div>

	</div>

	<div class="main-content">
				
		<?php require_once $content; ?>
		
		
		
		<br />
		<!-- Footer -->
		<footer class="main">
			
			&copy; 2016 <strong>Find-A-Nanny</strong>. Done by <a href="http://black.co.ke" target="_blank">Black | Dev & Design</a>
		
		</footer>
	</div>

	
	
</div>





	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="../assets/js/datatables/responsive/css/datatables.responsive.css">
	<link rel="stylesheet" href="../assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="../assets/js/select2/select2.css">
	
	<link rel="stylesheet" href="../assets/js/selectboxit/jquery.selectBoxIt.css">
	<link rel="stylesheet" href="../assets/js/daterangepicker/daterangepicker-bs3.css">
	<link rel="stylesheet" href="../assets/js/icheck/skins/minimal/_all.css">
	<link rel="stylesheet" href="../assets/js/icheck/skins/square/_all.css">
	<link rel="stylesheet" href="../assets/js/icheck/skins/flat/_all.css">
	<link rel="stylesheet" href="../assets/js/icheck/skins/futurico/futurico.css">
	<link rel="stylesheet" href="../assets/js/icheck/skins/polaris/polaris.css">
	
	<link rel="stylesheet" href="../assets/js/dropzone/dropzone.css">

	<!-- Bottom scripts (common) -->
	<script src="../assets/js/gsap/main-gsap.js"></script>
	<script src="../assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="../assets/js/bootstrap.js"></script>
	<script src="../assets/js/joinable.js"></script>
	<script src="../assets/js/resizeable.js"></script>
	<script src="../assets/js/neon-api.js"></script>
	<script src="../assets/js/jquery.dataTables.min.js"></script>
	<script src="../assets/js/datatables/TableTools.min.js"></script>


	<!-- Imported scripts on this page -->
	<script src="../assets/js/dataTables.bootstrap.js"></script>
	<script src="../assets/js/datatables/jquery.dataTables.columnFilter.js"></script>
	<script src="../assets/js/datatables/lodash.min.js"></script>
	<script src="../assets/js/datatables/responsive/js/datatables.responsive.js"></script>
	<script src="../assets/js/select2/select2.min.js"></script>

	
	<!-- Validation -->
	<script src="../assets/js/jquery.validate.min.js"></script>
	<script src="../assets/js/jquery.inputmask.bundle.min.js"></script>
	<!--Dropdown-->
	<script src="../assets/js/select2/select2.min.js"></script>
	<script src="../assets/js/bootstrap-tagsinput.min.js"></script>
	<script src="../assets/js/typeahead.min.js"></script>
	<script src="../assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="../assets/js/bootstrap-datepicker.js"></script>
	<script src="../assets/js/bootstrap-timepicker.min.js"></script>
	<script src="../assets/js/bootstrap-colorpicker.min.js"></script>
	<script src="../assets/js/daterangepicker/moment.min.js"></script>
	<script src="../assets/js/daterangepicker/daterangepicker.js"></script>
	<script src="../assets/js/jquery.multi-select.js"></script>
	<script src="../assets/js/icheck/icheck.min.js"></script>
	<script src="../assets/js/neon-chat.js"></script>
	
	<script src="../assets/js/fileinput.js"></script>
	<script src="../assets/js/dropzone/dropzone.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="../assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="../assets/js/neon-demo.js"></script>

</body>
</html>