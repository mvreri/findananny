<?php
$errorMessage = (isset($_GET['error']) && $_GET['error'] != '') ? $_GET['error'] : '&nbsp;';

$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

switch ($view) {
	case 'login' :
		$heading = 'Surreal School System | Login';
		$content = 'login_admin.php';
		$pageTitle = 'Surreal School System | Login';
		break;

	case 'forgotpassword' :
		$heading = 'Surreal School System | Forgot Password';
		$content = 'forgotpassword.php';
		$pageTitle = 'Surreal School System | Forgot Password';
		break;
		
	case 'register' :
		$heading = 'Surreal School System | Forgot Password';
		$content = 'signup.php';
		$pageTitle = 'Surreal School System | Forgot Password';
		break;
	
		
	default :
		$heading = 'Surreal School System | Login';
		$content = 'login_admin.php';
		$pageTitle = 'Surreal School System | Login';
}
	

require_once 'template.php';
?>