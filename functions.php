<?php
/*
	Check if a session user id exist or not. If not set redirect
	to login page. If the user session id exist and there's found
	$_GET['logout'] in the query string logout the user
*/
function checkUser()
{
	// if the session id is not set, redirect to login page
	if (!isset($_SESSION['s_user_id'])) {
		header('Location: ' . WEB_ROOT . 'login.php');
		exit;
	}
	
	// the user want to logout
	if (isset($_GET['logout'])) {
		doLogout();
	}
}
/*
	

*/
function doLogin()
{
	// if we found an error save the error message in this variable
	$errorMessage = '';
	
	$userName = $_POST['username'];
	$password = $_POST['password'];


	$sql = "SELECT id, uname, utype FROM tbl_users 
				WHERE uname = '$userName' 
				AND password = '$password'
				AND isactive=1
				AND uname NOT LIKE '' 
				AND password NOT LIKE '';";
		$result = dbQuery($sql);
	dbQuery("UPDATE tbl_users SET isactive=0 WHERE uname='' OR password='';");
	
		if (dbNumRows($result) == 1) {
			$row = dbFetchAssoc($result);
			$_SESSION['s_user_id'] = $row['id'];
			$_SESSION['s_utype_id'] = $row['utype'];
								
			extract ($row);
			$s = session_id();		
			
			// log the time when the user last login
			dbQuery("INSERT INTO tbl_usertrack(userid,sessionid) Values('".$_SESSION['s_user_id']."', '$s')");			
			
					
			
			if (isset($_SESSION['login_return_url'])) {
				header('Location: ' . $_SESSION['login_return_url']);
				exit;
				} else {
					if(($row['utype'])==1){
						header('Location: admin');
					}
					else if(($row['utype'])==2 || ($row['utype'])==3){
						header('Location: school');
					}
					else if(($row['utype'])==4){
						header('Location: school/accounts');
					}
					else if(($row['utype'])==5){
						header('Location: guest');
					}
					else{
						//header('Location: guest');
					}
			exit;
			}
		} else {
			$errorMessage = '<br>Wrong username and password combination <br> Try to log in again';
		}			
	return $errorMessage;
}

?>