
					
		<h2>Find Nannies and Housekeepers</h2>
		<br />
		
		<div class="panel panel-primary">
		
			
		
			<div class="panel-body">
		
				<form role="form" id="form1" method="post" class="validate" action="process.php?action=find" >
					
					<div class="form-group">
						<label class="control-label">Minimum Age </label>
		
						<input type="text" class="form-control" name="age" data-validate="required, number, minlength[2], maxlength[2]" data-message-required="Please specify a minimum  age." data-mask="99" data-numeric="true" data-numeric-align="left" placeholder="Enter a minimum age" />
					</div>
					
					<div class="form-group">
						<label class="control-label">Minimum Experience (Years)</label>
		
						<input type="text" class="form-control" name="yoe" data-validate="number, minlength[1], maxlength[2]" data-message-required="Please specify years of experience (minimum)." data-numeric="true" data-numeric-align="left" placeholder="Enter a the years of experience (minumum) e.g. 2" />
					</div>
					<div class="form-group">
								<label class="control-label">Specialization</label>
								
								<select name="specialization" class="form-control" class="selectboxit" data-first-option="false"  data-message-required="Please select a specialization.">
										<option></option>
                                        <option value="1">Nanny</option>
                                        <option value="2">Housekeeper</option>
                                        <option value="3">Nanny & Housekeeper</option>
								</select>
					</div>
					
					<div class="form-group">
						<label class="control-label">Budget</label>
		
						<input type="text" class="form-control" name="salary" data-validate="number, minlength[4], maxlength[5]" data-message-required="Please specify a budget." data-numeric="true" data-numeric-align="left" placeholder="Enter a the years of experience" />
					</div>
					
				
					<div class="form-group">
						<button type="submit" class="btn btn-success">Find</button>
						<button type="reset" class="btn">Reset</button>
					</div>
		
				</form>
		
			</div>
		
		</div>