<?php
require_once '../library/config.php';
require_once '../functions.php';

checkUser();
$errorMessage = (isset($_GET['error']) && $_GET['error'] != '') ? $_GET['error'] : '&nbsp;';

$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

switch ($view) {
	case 'listing' :
		$heading = 'Bureau | Start';
		$breadcrumb1 = 'Personnel';
		$breadcrumb2 = 'List All';
		$content = 'f_start_list.php';
		$pageTitle = 'Find-A-Nanny | Start';
		break;
		
	case 'activelisting' :
		$heading = 'Bureau | Start';
		$breadcrumb1 = 'Personnel';
		$breadcrumb2 = 'List Active';
		$content = 'f_start_list_active.php';
		$pageTitle = 'Find-A-Nanny | Start';
		break;
		
	case 'inactivelisting' :
		$heading = 'Bureau | Start';
		$breadcrumb1 = 'Personnel';
		$breadcrumb2 = 'List Inactive';
		$content = 'f_start_list_inactive.php';
		$pageTitle = 'Find-A-Nanny | Start';
		break;

	case 'newpersonnel' :
		$heading = 'Bureau | Downloads';
		$breadcrumb1 = 'Personnel';
		$breadcrumb2 = 'Add New';
		$content = 'f_new_personnel.php';
		$pageTitle = 'Bureau | Downloads';
		break;
		
	case 'newpersonnelbulk' :
		$heading = 'Bureau | Downloads';
		$breadcrumb1 = 'Personnel';
		$breadcrumb2 = 'Add New (Bulk)';
		$content = 'f_new_personnel_bulk.php';
		$pageTitle = 'Bureau | Downloads';
		break;
		
	default :
		$heading = 'Bureau | Start';
		$breadcrumb1 = 'Personnel';
		$breadcrumb2 = 'List All';
		$content = 'f_start_list.php';
		$pageTitle = 'Find-A-Nanny | Start';
}
	

require_once 'template.php';
?>