<div class="row">
		
		
		
		
			<!-- Raw Links -->
			<div class="col-md-12 col-sm-12 clearfix hidden-xs">
		
				<ul class="list-inline links-list pull-right">	
					<li>
						<a href="../login.php?view=login">
							Log Out <i class="entypo-logout right"></i>
						</a>
					</li>
				</ul>
		
			</div>
		
		</div>
		
		<hr />
		
					<ol class="breadcrumb bc-3" >
								<li>
						<a href="index.html"><i class="fa-home"></i>Home</a>
					</li>
							<li>
		
									<a href="tables-main.html"><?php echo $breadcrumb1; ?></a>
							</li>
						<li class="active">
		
									<strong><?php echo $breadcrumb2; ?></strong>
							</li>
							</ol>
					
		<h2>New Nannies and Housekeepers</h2>
		<br />
		
		<div class="panel panel-primary">
		
			<div class="panel-heading">
				<div class="panel-title">Please fill in all fields <small> <code> For the purpose of comprehensive searching</code></small></div>
			</div>
		
			<div class="panel-body">
		
				<form role="form" id="form1" method="post" class="validate" action="process.php?action=addnanny" enctype="multipart/form-data">
		
					<div class="form-group">
						<label class="control-label">Name</label>
		
						<input type="text" class="form-control" name="name" data-validate="required, minlength[4], maxlength[20]" data-message-required="Please specify a name." placeholder="Add a name e.g. John Mwangi" />
					</div>
					
					<div class="form-group">
						<label class="control-label">Year of Birth</label>
		
						<input type="text" class="form-control" name="yob" data-validate="required, number, minlength[4], maxlength[4]" data-message-required="Please specify a Year of Birth." data-mask="9999" data-numeric="true" data-numeric-align="left" placeholder="Enter a year of Birth" />
					</div>
					
					<div class="form-group">
						<label class="control-label">National ID. No.</label>
		
						<input type="text" class="form-control" name="natidno" data-validate="required, number, minlength[7], maxlength[8]" data-message-required="Please specify a National ID No." data-numeric="true" data-numeric-align="left" placeholder="Enter a National ID Number" />
					</div>
					
					<div class="form-group">
						<label class="control-label">Phone No.</label>
		
						<input type="text" class="form-control" name="phone" data-validate="required, number, minlength[10], maxlength[12]" data-message-required="Please specify a Phone No." data-numeric="true" data-numeric-align="left" placeholder="Enter a Phone Number" />
					</div>
					
					<div class="form-group">
						<label class="control-label">Experience (Years)</label>
		
						<input type="text" class="form-control" name="yoe" data-validate="required, number, minlength[1], maxlength[2]" data-message-required="Please specify years of experience." data-numeric="true" data-numeric-align="left" placeholder="Enter a the years of experience" />
					</div>
					<div class="form-group">
								<label class="control-label">Specialization</label>
								
								<select name="specialization" class="form-control" class="selectboxit" data-first-option="false" data-validate="required" data-message-required="Please select a specialization.">
										<option></option>
                                        <option value="1">Nanny</option>
                                        <option value="2">Housekeeper</option>
                                        <option value="3">Nanny & Housekeeper</option>
								</select>
					</div>
					
					<div class="form-group">
								<label class="control-label">Salary Range</label>
								
								<select name="salary" class="form-control" class="selectboxit" data-first-option="false" data-validate="required" data-message-required="Please select a salary range.">
										<option></option>
                                        <option value="1">KES. 10,000 to KES. 13,000</option>
										<option value="2">KES. 13,001 to KES. 15,000</option>
										<option value="3">KES. 15,001 to KES. 18,000</option>
										<option value="4">KES. 18,001 to KES. 21,000</option>
										<option value="5">Above KES. 21,000</option>
								</select>
					</div>
					<div class="form-group">
								<label class="control-label">Upload CV</label>
								
								<div class="fileinput fileinput-new" data-provides="fileinput">
										<span class="btn btn-info btn-file">
											<span class="fileinput-new">Select file</span>
											<span class="fileinput-exists">Change</span>
											<input type="file" name="cv" data-validate="required" data-message-required="Please upload a CV file.">
										</span>
										<span class="fileinput-filename"></span>
										<a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
									</div>
							</div>
					<div class="form-group">
								<label class="control-label">Upload Image</label>
								
								<div class="fileinput fileinput-new" data-provides="fileinput" >
										<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
											<img src="http://placehold.it/200x150" alt="...">
										</div>
										<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
										<div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="photo" accept="image/*" data-validate="required" data-message-required="Please upload a photo.">
											</span>
											<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
										</div>
									</div>
							</div>
	
					<div class="form-group">
						<button type="submit" class="btn btn-success">Done</button>
						<button type="reset" class="btn">Reset</button>
					</div>
		
				</form>
		
			</div>
		
		</div>