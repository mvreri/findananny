<div class="row">
		
		
		
		
			<!-- Raw Links -->
			<div class="col-md-12 col-sm-12 clearfix hidden-xs">
		
				<ul class="list-inline links-list pull-right">	
					<li>
						<a href="../login.php?view=login">
							Log Out <i class="entypo-logout right"></i>
						</a>
					</li>
				</ul>
		
			</div>
		
		</div>
		
		<hr />
		
					<ol class="breadcrumb bc-3" >
								<li>
						<a href="index.html"><i class="fa-home"></i>Home</a>
					</li>
							<li>
		
									<a href="tables-main.html"><?php echo $breadcrumb1; ?></a>
							</li>
						<li class="active">
		
									<strong><?php echo $breadcrumb2; ?></strong>
							</li>
							</ol>
					
		<h3>
			Drag n' Drop Bulk Upload
			<br />
			<small>The following is the structure for the Excel file you should upload<br> <strong>Name, Year of Birth, National ID Number, Phone Number, Years of Experience, Expected Salary, Specialization</strong><br> The specialization can only be "nanny", "housekeeper" or "nanny and housekeeper"<br><a href="nanny_template.xlsx" >Check out a template here</a></small>
		</h3>
		
		<br />
			
		<form action="process.php?action=addnannybulk" method="post" class="dropzone" id="dropzone_example"  enctype="multipart/form-data">
			<div class="fallback">
				<input name="file" type="file" multiple />
			</div>
		</form>
		
		<div id="dze_info" class="hidden">
			
			<br />
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="panel-title">Dropzone Uploaded Files Info</div>
				</div>
				
				<table class="table table-bordered">
					<thead>
						<tr>
							<th width="40%">File name</th>
							<th width="15%">Size</th>
							<th width="15%">Type</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="4"></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>