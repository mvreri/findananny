<?php
if (isset($_GET['specialization']) && (int)$_GET['specialization'] >= 0) {
	$specialization = (int)$_GET['specialization'];
	$queryString = "&specialization=$specialization";
} else {
	$specialization = 0;
	$queryString = '';
	header('Location: index.php?view=home&error=' . urlencode('No specialization specified')); 
}
if (isset($_GET['age']) && (int)$_GET['age'] >= 0) {
	$age = (int)$_GET['age'];
	$queryString = "&age=$age";
} else {
	$age = 0;
	$queryString = '';
	header('Location: index.php?view=home&error=' . urlencode('No age specified')); 
}
if (isset($_GET['experience']) && (int)$_GET['experience'] >= 0) {
	$experience = (int)$_GET['experience'];
	$queryString = "&experience=$experience";
} else {
	$experience = 0;
	$queryString = '';
	header('Location: index.php?view=home&error=' . urlencode('No experience specified')); 
}
if (isset($_GET['salary']) && (int)$_GET['salary'] >= 0) {
	$salary = (int)$_GET['salary'];
	$queryString = "&salary=$salary";
} else {
	$salary = 0;
	$queryString = '';
	header('Location: index.php?view=home&error=' . urlencode('No salary specified')); 
}
 ?>
 <div class="row">
		
		
		
		

					
		<h2>Nannies & Housekeepers</h2>
		

				
		<h3>Available </h3>
		<br />
		
		<table class="table table-bordered datatable" id="table-nannies">
			<thead>
				<tr>
					<th>Name</th>
					<th>Experience</th>
					<th>Age</th>
					<th>CV</th>
					<th>Photo</th>
				</tr>
			</thead>
			<tbody>
			<?php
			$res=dbQuery("SELECT n.name name, n.experience exp, n.cv resumee, n.photo picha, s.minsalary minsal, s.maxsalary maxsal, sp.name spec, YEAR(CURDATE())-n.yob age
																FROM tbl_nanny n
																INNER JOIN tbl_salary s
																ON s.id=n.salaryid
																INNER JOIN tbl_specialization sp
																ON sp.id = n.specializationid
																WHERE n.experience>=$experience
																AND $salary BETWEEN s.minsalary AND s.maxsalary
																AND $specialization >= sp.id
																AND $age <= YEAR(CURDATE())-n.yob
																ORDER BY n.id desc;");
												while ($row=dbFetchAssoc($res)){
													extract($row);
											
                                                ?>
				<tr class="odd gradeX">
					<td><?php echo $name; ?></td>
					<td><?php echo $exp; ?></td>
					<td><?php echo $age; ?></td>					
					<td><a href="../uploadscv/<?php echo $resumee; ?>"><?php echo $resumee; ?></a></td>
					<td><img src="../uploads/<?php echo $picha; ?>" width="175" height="200" /></td>
				</tr>
				 <?php
					}
				?>
			</tbody>
			<tfoot>
				<tr>
					<th>Name</th>
					<th>Experience</th>
					<th>Age</th>
					<th>CV</th>
					<th>Photo</th>
				</tr>
			</tfoot>
		</table>
		<script type="text/javascript">
			jQuery(document).ready(function($)
			{
				var table = $("#table-nannies").dataTable({
					"sPaginationType": "bootstrap",
					"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
					"oTableTools": {
					},
					
				});
			});
				
		</script>